use std::convert::From;

mod test_traits;
use crate::test_traits::*;

mod taiga_logging;

#[derive(Debug)]
struct Number {
    value: i32,
    value2: i32
}

impl From<i32> for Number {
    fn from(item: i32) -> Self {
        Number { value: item, value2: item+1 }
    }
}

//----------------------------

enum MyEnum {
    Truc( u32 ),
    Machin( u32, String ),
}
    
fn do_something( d: MyEnum ) -> u32 {
    match d {
        MyEnum::Truc( a ) => { println!( "Truc"); a },
        MyEnum::Machin( a, b ) => { println!( "Machin: {}, {}", a, b ); a },
    }
}

fn test_enum() {
    let d = MyEnum::Machin( 10, String::from( "aze" ) );
   println!(  "do_something(): {}", do_something( d ) );
   
   let dice_roll = 9;
   match dice_roll {
       3 => println!( "three" ),
       7 => println!( "seven" ),
       azeaze => println!( "this is something else... {}", azeaze ),
      }
}

//----------------------------

fn main() {
    println!("Hello, world!");

    test_enum();

    // Test 1
    let num = Number::from(30);
    println!("My number is {:?}", num);

    // Test 2
    let int = 5;
    // Try removing the type declaration
    let num: Number = int.into();
    println!("My number is {:?}", num);


    // Test traits
    let tweet = Tweet {
        username: String::from("horse_ebooks"),
        content: String::from(
            "of course, as you probably already know, people",
        ),
        reply: false,
        retweet: false,
    };

    println!("1 new tweet: {}", tweet.summarize());

    let email = EMail { sender: String::from( "him" ), };
    println!("1 new email: {}", email.summarize());

    notify( &tweet );
    notify( &email );
}
