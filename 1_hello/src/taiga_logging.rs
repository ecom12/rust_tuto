
pub struct LogData {
//        struct timespec timestamp;
    timestamp: u64,
    task_name: String,
    pub app_name: String,
    pub severity: u32,
    pub function: u32,
    pub filename: String,
    pub line: u32,
    pub message: String,
}

fn log_warning<T: PublishLog>( lg: T, ld: &LogData ) -> Option<String>
{
    //ld.task_name = String::from( "some_task" );
    lg.publish( ld );
    None
}


pub trait PublishLog {
    fn publish( &self, ld: &LogData ) -> Option<String> {
        println!( "[{}] {}", ld.app_name, ld.message );
        None
    }
}

mod rtac {
    pub struct RtacLogServer {
        ip: String,
        port: u32,
    }

    impl crate::taiga_logging::PublishLog for RtacLogServer {
        fn publish( &self, ld: &crate::taiga_logging::LogData ) -> Option<String> {
            println!( "Logging to {}@{}: [{}] {}", self.port, self.ip, ld.app_name, ld.message );
            None
        }
    }
}
